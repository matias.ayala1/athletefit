from flask import render_template, Response, request, Blueprint
import cv2
import mediapipe as mp
import numpy as np
from math import acos, degrees #To calculate the angle
import mancuerna
import squat
import push_ups
views = Blueprint(__name__ , "")


global count, up, down, left_arm_counter, right_arm_counter
count = 0
up = False 
down = False 

#Mancuerna
global up_right, down_right, up_left, down_left
left_arm_counter = 0
right_arm_counter = 0
up_right =  False
down_right = False
up_left = False
down_left = False

#Alerta
global alert,alert_right, alert_left
alert = 0
alert_right = 0
alert_left = 0


def gen_frames(total_reps, mov):  # generate frame by frame from camera
    mp_pose = mp.solutions.pose
    cap = cv2.VideoCapture(0)
    
    with mp_pose.Pose(
        static_image_mode=False) as pose:
        while True: 
            ret, frame = cap.read()
            if ret == False:
                break
            frame = cv2.flip(frame, 1) #en directoo
            height, width, _ = frame.shape
            frame_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            results = pose.process(frame_rgb)

            if results.pose_landmarks is not None:
                if mov == "mancuerna":
                    mancuerna.mancuerna_movement(results,total_reps,height, width, frame)
                    
                elif mov  == "sentadilla":
                    squat.check_squat(results,total_reps, height, width, frame)

                elif mov  == "push_ups":
                    push_ups.push_ups_moment(results, total_reps,height, width, frame)

            (flag, encodedImage) = cv2.imencode(".jpg", frame)
            if not flag:
                continue
            yield(b'--frame\r\n'b'Content-Type: image/jpeg\r\n\r\n' + bytearray(encodedImage) + b'\r\n')
            


@views.route("/squats_video", methods = ['POST', 'GET'])
def squats_video():
    output = request.form.to_dict()
    reps = output["reps"]
    return Response(gen_frames(int(reps),"sentadilla"), mimetype='multipart/x-mixed-replace; boundary=frame')

@views.route("/mancuerna_video", methods = ['POST', 'GET'])
def mancuerna_video():
    output = request.form.to_dict()
    reps = output["reps"]
    return Response(gen_frames(int(reps),"mancuerna"), mimetype='multipart/x-mixed-replace; boundary=frame')

@views.route("/push_ups_video", methods = ['POST', 'GET'])
def push_ups_video():
    output = request.form.to_dict()
    reps = output["reps"]
    return Response(gen_frames(int(reps),"push_ups"), mimetype='multipart/x-mixed-replace; boundary=frame')

@views.route("/")
def home():
    return render_template("index.html")


@views.route("/squats.html")
def squats():
    return render_template("squats.html")

@views.route("/mancuernas.html")
def mancuernas():
    return render_template("mancuernas.html")

@views.route("/push_ups.html")
def push_ups():
    return render_template("push_ups.html")




