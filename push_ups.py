import cv2
import numpy as np
from math import acos, degrees
from mancuerna import counter_mancuerna
import winsound
duration = 1000  # milliseconds
freq = 440  # Hz

global up_right, down_right, up_left, down_left, left_arm_counter, right_arm_counter
up_right =  False
down_right = False
up_left = False
down_left = False
left_arm_counter = 0
right_arm_counter = 0

#Alerta
global alert, alert_right, alert_left
alert = 0


def push_ups_movement(results, total_reps, height, width, frame):
    global up_right
    global down_right
    global up_left
    global down_left
    global right_arm_counter
    global left_arm_counter
    global alert_right
    global alert_left
    temp_count_right = right_arm_counter
    temp_count_left = left_arm_counter


    #Right
    #Shoulder
    x1 = int(results.pose_landmarks.landmark[12].x * width)
    y1 = int(results.pose_landmarks.landmark[12].y * height)
    #elbow
    x2 = int(results.pose_landmarks.landmark[14].x * width)
    y2 = int(results.pose_landmarks.landmark[14].y * height)
    #wrist
    x3 = int(results.pose_landmarks.landmark[16].x * width)
    y3 = int(results.pose_landmarks.landmark[16].y * height)
    
    #Points
    p1_shoulder = np.array([x1, y1])
    p2_elbow = np.array([x2,y2])
    p3_wrist = np.array([x3,y3])

    #Lineas
    l1_s_e = np.linalg.norm(p1_shoulder - p2_elbow)
    l2_e_w = np.linalg.norm(p2_elbow - p3_wrist)
    l3_s_w = np.linalg.norm(p1_shoulder - p3_wrist)
    angle_right = degrees(acos((l1_s_e**2 + l2_e_w**2 - l3_s_w**2) / (2 * l1_s_e * l2_e_w)))

    #Left
    #Shoulder
    x1_l = int(results.pose_landmarks.landmark[11].x * width)
    y1_l = int(results.pose_landmarks.landmark[11].y * height)
    #elbow
    x2_l = int(results.pose_landmarks.landmark[13].x * width)
    y2_l = int(results.pose_landmarks.landmark[13].y * height)
    #wrist
    x3_l = int(results.pose_landmarks.landmark[15].x * width)
    y3_l = int(results.pose_landmarks.landmark[15].y * height)


    #Points
    p1_shoulder_left = np.array([x1_l, y1_l])
    p2_elbow_left = np.array([x2_l,y2_l])
    p3_wrist_left = np.array([x3_l,y3_l])
    
    #2nd frame
    aux_image = np.zeros(frame.shape, np.uint8)

    #Lineas
    l1_s_e_left = np.linalg.norm(p1_shoulder_left - p2_elbow_left)
    l2_e_w_left = np.linalg.norm(p2_elbow_left - p3_wrist_left)
    l3_s_w_left = np.linalg.norm(p1_shoulder_left - p3_wrist_left)
    angle_left = degrees(acos((l1_s_e_left**2 + l2_e_w_left**2 - l3_s_w_left**2) / (2 * l1_s_e_left * l2_e_w_left)))
    
    #Counter 
    #counter_mancuerna(temp_count,arm,angle,angle_restriction, angle_limit)

    #counter_mancuerna(right_arm_counter,"right",angle_right,160,120)
    counter_mancuerna(left_arm_counter,"left",angle_left,160,120)

    if right_arm_counter == total_reps and alert_right == 0:
        alert_right = total_reps
        winsound.Beep(freq, duration)
    if left_arm_counter == total_reps and alert_left == 0:
        alert_left = total_reps

        winsound.Beep(freq, duration)
        
    #Visualization
    #2nd frame
    output = cv2.addWeighted(frame, 1, aux_image, 0.8, 0)
    
    #Angle
    cv2.putText(output, str(int(angle_left)), (x2_l + 30, y2_l), 1, 1.5, (0, 0, 0), 2)
    cv2.putText(output, str(int(angle_right)), (x2 + 30, y2), 1, 1.5, (0, 0, 0), 2)
    
    #Relleno
    contours_left = np.array([p1_shoulder_left, p2_elbow_left, p3_wrist_left])
    contours_right = np.array([p1_shoulder, p2_elbow, p3_wrist])
    cv2.fillPoly(output, pts=[contours_left], color=(128, 0, 250))
    cv2.fillPoly(output, pts=[contours_right], color=(128, 0, 250)) 

    #Lineas
    cv2.line(output, p1_shoulder_left, p2_elbow_left, (255, 255, 0), 10)
    cv2.line(output, p2_elbow_left, p3_wrist_left, (255, 255, 0), 10)
    cv2.line(output, p1_shoulder_left, p3_wrist_left, (255, 255, 0), 5) 
    

    
    #Counter visualization
    cv2.rectangle(frame, (0, 0), (80, 60), (255, 255, 255), -1) #left rectangle
    cv2.putText(frame, str(left_arm_counter), (10, 50), 1, 3.5, (0, 0, 0), 2)#show left counter
    #cv2.rectangle(frame, (284, 0), (384, 60), (255, 255, 255), -1) #right rectangle
    #cv2.putText(frame, str(right_arm_counter), (280, 50), 1, 3.5, (0, 0, 0), 2)#show right counter
    
    #
    cv2.rectangle(output, (0, 0), (80, 60), (255, 255, 255), -1) #left rectangle
    cv2.putText(output, str(left_arm_counter), (10, 50), 1, 3.5, (0, 0, 0), 2)#show left counter
    #cv2.rectangle(output, (284, 0), (384, 60), (255, 255, 255), -1) #right rectangle
    #cv2.putText(output, str(right_arm_counter), (280, 50), 1, 3.5, (0, 0, 0), 2)#show right counter
    
    
    #if alert_left == total_reps:
        #cv2.putText(frame, "Left arm done!!!", (0, 100), 1, 1, (255, 255, 255), 2)#show left counter
        #cv2.putText(output, "Left arm done!!!", (0, 100), 1, 1, (255, 255, 255), 2)#show left counter
    if alert_right == total_reps:
        cv2.putText(frame, "Right arm done!!!", (200, 100), 1, 1, (255, 255, 255), 2)#show left counter
        cv2.putText(output, "Right arm done!!!", (200, 100), 1, 1, (255, 255, 255), 2)#show left counter
    cv2.imshow("output", output)  
