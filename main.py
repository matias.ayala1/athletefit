import cv2
import mediapipe as mp
import numpy as np
from math import acos, degrees #To calculate the angle
import squat
import mancuerna


mp_drawing = mp.solutions.drawing_utils
mp_pose = mp.solutions.pose
cap = cv2.VideoCapture("video_003.mp4")
mov = "mancuerna"
with mp_pose.Pose(
    static_image_mode=False) as pose:
    while True: 
        ret, frame = cap.read()
        if ret == False:
            break
        #ret, buffer = cv2.imencode('.jpg', frame)
        #frame = buffer.tobytes()
        #lambda: (yield) (b'--frame\r\n'b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')  # concat frame one by one and show result        
        

        frame = cv2.flip(frame, 1) #en directoo
        height, width, _ = frame.shape
        frame_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        results = pose.process(frame_rgb)
        if results.pose_landmarks is not None:
            if mov == "mancuerna":
                mancuerna.mancuerna_movement(height, width, frame)
                
            elif mov  == "sentadilla":
                squat.check_squat(height, width, frame)


           
        cv2.imshow("Frame", frame) #en directo
        if cv2.waitKey(1) & 0xFF == 27:
            break
cap.release()
cv2.destroyAllWindows() 
