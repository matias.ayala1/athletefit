import cv2
import numpy as np
from math import acos, degrees
import winsound
duration = 1000  # milliseconds
freq = 440  # Hz


global count, up, down
count = 0
up = False 
down = False 

#Alerta
global alert
alert = 0

def check_squat(results, total_reps, height, width,frame):
    global count
    global up
    global down
    global alert
    temp_count = count
    
    x1 = int(results.pose_landmarks.landmark[24].x * width)
    y1 = int(results.pose_landmarks.landmark[24].y * height)#esta
    x2 = int(results.pose_landmarks.landmark[26].x * width)#esta
    y2 = int(results.pose_landmarks.landmark[26].y * height)
    x3 = int(results.pose_landmarks.landmark[28].x * width)
    y3 = int(results.pose_landmarks.landmark[28].y * height)
    x4 = int(results.pose_landmarks.landmark[12].x * width)
    y4 = int(results.pose_landmarks.landmark[12].y * height)
    p1 = np.array([x1, y1])
    p2 = np.array([x2, y2])
    p3 = np.array([x3, y3])
    p4 = np.array([x4, y4])
    l1 = np.linalg.norm(p1 - p2)
    l2 = np.linalg.norm(p2 - p3)
    l3 = np.linalg.norm(p1 - p3)
    l4 = np.linalg.norm(p4 - p2)
    l5 = np.linalg.norm(p1 - p4)
    l4 = np.linalg.norm(p1 - p2)
    
    # Calcular el ángulo
    angle = degrees(acos((l1**2 + l2**2 - l3**2) / (2 * l1 * l2)))
    #angle1 = degrees(acos((l4**2 + l3**2 - l5**2) / (2 * l4 * l3)))
    #Para contar sentadillas
    """
    if angle >= 160:
        print(up)
        up = True
        print(up)
    if up == True and down == False and angle <= 70:
        down = True
    if up == True and down == True :
        print(angle)
        temp_count += 1
        count = temp_count
        up = False
        down = False
        print("count: ", count) 
        """
    #counter_exercise(temp_count,angle,up,down, angle_restriction, angle_limit)
    counter_squat(temp_count,angle,160,70)

    if count == total_reps and alert == 0:
        alert = total_reps
        winsound.Beep(freq, duration)
        

    # Visualización
    aux_image = np.zeros(frame.shape, np.uint8)
    cv2.line(aux_image, (x1, y1), (x2, y2), (255, 255, 0), 20)
    cv2.line(aux_image, (x2, y2), (x3, y3), (255, 255, 0), 20)
    cv2.line(aux_image, (x1, y1), (x3, y3), (255, 255, 0), 5)

    ##
    cv2.line(aux_image, (x4, y4), (x1, y1), (255, 255, 0), 20) #Para que no se vean las lineas
    cv2.line(aux_image, (x1, y1), (x2, y2), (255, 255, 0), 20) #Esta linea esta arriba
    cv2.line(aux_image, (x4, y4), (x2, y2), (255, 255, 0), 20) #Para que no se vean las lineas

    contours = np.array([[x1, y1], [x2, y2], [x3, y3]])
    contours2 = np.array([[x4, y4], [x1, y1], [x2, y2]])##cambio
    cv2.fillPoly(aux_image, pts=[contours], color=(128, 0, 250)) #Descomentar
    cv2.fillPoly(aux_image, pts=[contours2], color=(169, 70, 100))#cambio
    output = cv2.addWeighted(frame, 1, aux_image, 0.8, 0)
    
    cv2.circle(output, (x1, y1), 6, (0, 255, 255), 4)
    cv2.circle(output, (x2, y2), 6, (128, 0, 250), 4)
    cv2.circle(output, (x3, y3), 6, (255, 191, 0), 4)
    cv2.circle(output, (x4, y4), 6, (169, 150, 200), 4) 

    #contador
    cv2.rectangle(output, (0, 0), (60, 60), (255, 255, 255), -1) #Visualoziar rect
    #cv2.putText(output, str(int(angle)), (x2 + 30, y2), 1, 1.5, (128, 0, 250), 2)
    cv2.putText(output, str(int(angle)), (x1 + 30, y1), 1, 1.5, (128, 0, 250), 2)
    cv2.putText(output, str(count), (10, 50), 1, 3.5, (0, 0, 0), 2)#Para contar
    
    cv2.rectangle(frame, (0, 0), (60, 60), (255, 255, 255), -1) #Visualoziar rect 
    cv2.putText(frame, str(count), (10, 50), 1, 3.5, (0, 0, 0), 2)#Para contar    
    if alert == total_reps:
        cv2.putText(output, "Total Squats done!!!", (0, 100), 1, 1, (255, 255, 255), 2)   
        cv2.putText(frame, "Total Squats done!!!", (0, 100), 1, 1, (255, 255, 255), 2)   
        
    cv2.imshow("output", output)


def counter_squat(temp_count,angle,angle_restriction, angle_limit):
    global up
    global down
    global count
    if angle >= angle_restriction:
        up = True
    if up == True and down == False and angle <= angle_limit:
        down = True
    if up == True and down == True and angle >= angle_restriction:
        temp_count += 1
        count = temp_count
        up = False
        down = False
        